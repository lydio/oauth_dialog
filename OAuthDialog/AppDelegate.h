//
//  AppDelegate.h
//  OAuthDialog
//
//  Created by apple on 9/29/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppRoot.h"


@interface AppDelegate : AppRoot //UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

