//
//  GoogleLoginWrapper.m
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "GoogleLoginWrapper.h"
#import "LoginResult.h"

#import <Google/SignIn.h>


@interface GoogleLoginWrapper () <GIDSignInDelegate, GIDSignInUIDelegate>
@property (strong, nonatomic) UIWindow *googleWindow;
@end


@implementation GoogleLoginWrapper

#pragma mark - AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    //[GIDSignIn sharedInstance].clientID = [[self infoPlist_internal] objectForKey:@"CLIENT_ID"]; это фикс - не пригодился
    
    return YES;
}


- (void)applicationDidBecomeActive: (UIApplication *)application {
    
    [GIDSignIn sharedInstance].uiDelegate = self;
}


- (BOOL) application:(UIApplication *)app
             openURL:(NSURL *)url
             options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    
    BOOL result = [[GIDSignIn sharedInstance] handleURL:url
                                      sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                             annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
    return result;
}


#pragma mark - LoginWrapper

- (NSDictionary *) infoPlist_internal {
    return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle mainBundle] pathForResource: @"GoogleService-Info" ofType:@"plist"]];
}

- (NSDictionary *) infoPlist_testing {
    
    NSDictionary *dict_internal = [self infoPlist_internal];
    NSString *scheme = [dict_internal objectForKey:@"REVERSED_CLIENT_ID"];
    NSDictionary *dict_testing = @{@"CFBundleURLTypes": @[@{@"CFBundleURLSchemes": @[scheme]}]};
    
    return dict_testing;
}

- (NSString *) siteName {
    return @"Google";
}


- (BOOL) nativeApplicationIsInstalled {
    
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: @"gplus://"]];
}

- (NSString *) urlSchemePrefix {
    
    return @"com.google";
}


- (void) setLoginBehavior: (LoginBehaviorEnum) behavior {
    /*
    in Google SDK is possible only one behavior - standard google login page inside webview
    */
}


- (UIControl *)loginButton {

    static GIDSignInButton *loginButton;
    if(!loginButton) {
        
        loginButton = [GIDSignInButton new];
        
        //signIn.shouldFetchBasicProfile = YES; // yes by default (include id, name, email, image)
        
        /*
         access to additional info  
         
         GIDSignIn scopes is like Facebook permisions,
         all scopes must be valid, delegate not work if scopes are wrong (may be is a bug).
         
         @"https://www.googleapis.com/auth/plus.me"
         @"https://www.googleapis.com/auth/plus.login"
        */
        
        GIDSignIn *signIn = [GIDSignIn sharedInstance];
        NSArray *defaultScopes = signIn.scopes;
        signIn.scopes = [defaultScopes arrayByAddingObjectsFromArray:@[@"https://www.googleapis.com/auth/plus.me"]];
    }
    
    return loginButton;
}


- (void) getUserInfo  { //main user info is grabbed by default, this request for 'scopes' only

    NSString *accessToken = [GIDSignIn sharedInstance].currentUser.authentication.accessToken;
    if(accessToken) {
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
            
            NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v1/userinfo?access_token=%@", accessToken];
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
            
            id json = nil;
            NSError *error = nil;
            
            @try {

                json = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingAllowFragments error: &error];
                
            } @finally {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    LoginResult *loginResult = [LoginResult sharedResult];
                    
                    if(json && !error) {
                    
                        loginResult.gender = [json objectForKey:@"gender"];
                        loginResult.birthday = [json objectForKey:@"birthday"];
                        //loginResult.imageUrl = [json objectForKey:@"picture"];
                    }
                    
                    //NSLog(@"%@", [loginResult toDictionary]);
                    [loginResult reloadAndNotify];
                });
            }
        });
    }
}


#pragma martk - GIDSignInDelegate & GIDSignInUIDelegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    // Perform any operations on signed in user here.
    LoginResult *loginResult = [LoginResult sharedResult];
    
    loginResult.siteName = [self siteName];
    loginResult.errorMessage = (error ? LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE : nil); // ? nil user without error
    
    if (!loginResult.errorMessage) {
        
        NSString *format;
        
        loginResult.fullName = user.profile.name;
        loginResult.nameComponent_1 = user.profile.givenName;
        loginResult.nameComponent_2 = user.profile.givenName;
        loginResult.email = user.profile.email;
        loginResult.accountIdentifier = user.userID;
        
        if(user.profile.hasImage) {
            loginResult.imageUrl = [[user.profile imageURLWithDimension: 512] absoluteString];
        }
        
        if(loginResult.accountIdentifier) {
            format = @"https://plus.google.com/%@/";
            loginResult.pageUrl = [NSString stringWithFormat: format, loginResult.accountIdentifier];
        }
        
        loginResult.errorMessage = nil; // login success
        
        [self getUserInfo]; // try once get additional info: gender, birthday etc
        
    } else {
        
        [loginResult reloadAndNotify];
    }
}


- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    // Perform any operations when the user disconnects from app here.
}


- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
    //[myActivityIndicator stopAnimating];
}


// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    
    if(!self.googleWindow) {
        
        UIWindow *googleWindow = [[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
        googleWindow.backgroundColor = [UIColor colorWithWhite: 0 alpha: 0.25];
        googleWindow.windowLevel = UIWindowLevelAlert;
        googleWindow.rootViewController = viewController; // SFSafariViewController
    
        self.googleWindow = googleWindow;
        [googleWindow makeKeyAndVisible];
    }
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    
    if(self.googleWindow) {
        self.googleWindow.rootViewController = nil;
        [self.googleWindow resignKeyWindow];
        self.googleWindow = nil;
    }
}



@end
