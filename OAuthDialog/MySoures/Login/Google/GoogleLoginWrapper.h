//
//  GoogleLoginWrapper.h
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AppRoot.h"


@class GIDSignInButton;

@interface GoogleLoginWrapper : UIResponder <UIApplicationDelegate, LoginWrapper>

@end
