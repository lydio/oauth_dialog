//
//  LoginView.h
//  OAuthDialog
//
//  Created by apple on 9/30/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView 

@property (nonatomic, strong, nonnull) NSArray <UIButton *> *allButtons;

@end
