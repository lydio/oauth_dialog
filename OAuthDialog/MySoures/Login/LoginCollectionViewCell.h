//
//  LoginCollectionViewCell.h
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIButton *button;

@end
