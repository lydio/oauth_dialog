//
//  FacebookLoginWrapper.m
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "FacebookLoginWrapper.h"
#import "LoginResult.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface FacebookLoginWrapper () <FBSDKLoginButtonDelegate>

@end

@implementation FacebookLoginWrapper


#pragma mark - AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    return YES;
}


- (void)applicationDidBecomeActive: (UIApplication *)application {
    [FBSDKAppEvents activateApp];
}


- (BOOL) application:(UIApplication *)app
             openURL:(NSURL *)url
             options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    
    //NSString *sourceApplicationKey = @"UIApplicationOpenURLOptionsSourceApplicationKey";
    //NSString *annotationKey = @"UIApplicationOpenURLOptionsAnnotationKey";
    
    BOOL result = [[FBSDKApplicationDelegate sharedInstance] application:app
                                                                 openURL:url
                                                       sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                              annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    
    return result;
}


#pragma mark - LoginWrapper

- (NSDictionary *) infoPlist_testing {
    return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle mainBundle] pathForResource: @"FacebookInfo" ofType:@"plist"]];
}

- (NSString *) siteName {
    return @"Facebook";
}


- (BOOL) nativeApplicationIsInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: @"fb://"]];
}

- (NSString *) urlSchemePrefix {
    return @"fb";
}


- (void) setLoginBehavior: (LoginBehaviorEnum) behavior {
    
    FBSDKLoginBehavior fbsdkBehavior;
    
    switch (behavior) {
            case LoginBehaviorWeb: fbsdkBehavior = FBSDKLoginBehaviorWeb;
            break;
            case LoginBehaviorBrowser: fbsdkBehavior = FBSDKLoginBehaviorBrowser;
            break;
            case LoginBehaviorNative: fbsdkBehavior = FBSDKLoginBehaviorNative;
            break;
        default: fbsdkBehavior = FBSDKLoginBehaviorWeb;
            break;
    }
    
    [(FBSDKLoginButton *)[self loginButton] setLoginBehavior: fbsdkBehavior];
}


- (UIButton *)loginButton {

    static FBSDKLoginButton *loginButton;
    if(!loginButton) {
        
        loginButton = [FBSDKLoginButton new];
        [loginButton setLoginBehavior: FBSDKLoginBehaviorWeb]; // by default, not change it here
        loginButton.delegate = self;
        
        // 1. если неправильно задать ключи, отменит запрос сразу, даже не коннектясь с фэйсбуком
        // 2. убедиться что это свойство нужно задавать(?), 'email' и 'public_profile' вроде разрешает и так
         loginButton.readPermissions =
            @[@"public_profile",
              @"email",
              @"user_birthday"];
    }
    
    return loginButton;
}


- (void) getUserInfo {
    
    if([FBSDKAccessToken currentAccessToken]) {
        
        NSDictionary *parameters =  @{@"fields": @"name, first_name, last_name, gender, birthday, picture.type(large), email, id"};
        FBSDKGraphRequest *graphRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters];
        
        [graphRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            
            LoginResult *loginResult = [LoginResult sharedResult];
            
            if (!error && [result isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *userInfo = (NSDictionary *)result;
                NSString *format;
                
                loginResult.fullName = userInfo[@"name"];
                loginResult.nameComponent_1 = userInfo[@"first_name"];
                loginResult.nameComponent_2 = userInfo[@"last_name"];
                loginResult.gender = userInfo[@"gender"];
                loginResult.birthday = userInfo[@"birthday"];
                loginResult.email = userInfo[@"email"];
                loginResult.accountIdentifier = userInfo[@"id"];
                
                if(loginResult.accountIdentifier) {
                    format = @"https://www.facebook.com/app_scoped_user_id/%@/";
                    loginResult.pageUrl = [NSString stringWithFormat: format, loginResult.accountIdentifier];
                }
                
                @try {
                    loginResult.imageUrl = userInfo[@"picture"][@"data"][@"url"];
                    
                } @catch (id error) {
                    /*if(loginResult.accountIdentifier) {
                        format = @"https://graph.facebook.com/%@/picture?type=large";
                        loginResult.imageUrl = [NSString stringWithFormat: format, loginResult.accountIdentifier];
                    }*/
                }
                
                loginResult.errorMessage = nil;
                
            } else {
                loginResult.errorMessage = LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE;
            }
            
            //NSLog(@"%@", [loginResult toDictionary]);
            [loginResult reloadAndNotify];
        }];
        
    } else {
        
        LoginResult *loginResult = [LoginResult sharedResult];
        loginResult.errorMessage = LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE;
        [loginResult reloadAndNotify];
    }
}



#pragma mark - FBSDKLoginButtonDelegate

- (BOOL) loginButtonWillLogin:(FBSDKLoginButton *)loginButton {

    /* 
     Может быть как то сохранять аргументы передаваемые по протоколу UIAppDelegate,
     и запускать в нужной последовательности в этом месте ?
     Это чтобы все, кроме кнопки, классы и синглтоны (!) инитить только здесь,
     так как использовано будет только одно API, остальные просто в памяти сидят
     */
    return YES;
}


    - (void) loginButton: (FBSDKLoginButton *)loginButton
   didCompleteWithResult: (FBSDKLoginManagerLoginResult *) result
                   error: (NSError *)error {
        
        
        LoginResult *loginResult = [LoginResult sharedResult];
        
        loginResult.siteName = [self siteName];
        loginResult.errorMessage =
            (error ? LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE :
             (result.isCancelled ? LOGIN_RESULT_RECOMMENDED_CANCEL_MESSAGE :
              nil));
        
        if(!loginResult.errorMessage) {
            
            [self getUserInfo]; // here result can be failed too
        
        } else {
            
            [loginResult reloadAndNotify]; // login failed or cancelled
        }
}


- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {

}


@end
