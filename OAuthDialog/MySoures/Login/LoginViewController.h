//
//  LoginViewController.h
//  OAuthDialog
//
//  Created by apple on 9/30/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LoginView;
@interface LoginViewController : UIViewController

@property (nonatomic, readonly) LoginView *loginView;

@end
