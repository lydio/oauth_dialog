//
//  LoginCollectionViewCell.m
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "LoginCollectionViewCell.h"


@interface LoginCollectionViewCell ()

@end


@implementation LoginCollectionViewCell
@synthesize button = _button;



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}



- (void) layoutSubviews {

    if(self.button) {
        self.button.frame = self.bounds;
    }
}


- (void) setButton:(UIButton *)button {

    if(_button == button) {
        // do nothing
    } else {
    
        if(_button) {
            [_button removeFromSuperview];
            _button = nil;
        }
        
        if(button) {
            [self addSubview: button];
            _button = button;
            
            [self layoutSubviews];
        }
    }
}



@end


