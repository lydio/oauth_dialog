//
//  LinkedinLoginWrapper.h
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "AppRoot.h"


@interface LinkedinLoginWrapper : NSObject <UIApplicationDelegate, LoginWrapper>

@property (nonatomic, copy) NSString *urlSchemePrefix;

@end
