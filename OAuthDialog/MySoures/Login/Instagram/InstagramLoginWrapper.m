//
//  InstagramLoginWrapper.m
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "InstagramLoginWrapper.h"
#import "LoginResult.h"


// local setting stored in local InstagramInfo.plist, needed only for this class
#define kCLIENT_ID @"ClientId"
#define kCLIENT_SECRET @"ClientSecret" // on release is better to remove secret from plist and put it inside code
#define kREDIRECT_URL @"RedirectUrl" // registered redirect URL must be https://localhost:8080, !instagram not support custom ios url schemes


@interface InstagramLoginWrapper () <UIWebViewDelegate>

@property (nonatomic) LoginBehaviorEnum loginBehavior;
@property (nonatomic, strong) UIWindow *loginWindow; // container for UIWebView

@property (nonatomic, copy) NSString *code; // authorization code (first login step)
@property (nonatomic, copy) NSString *token; // access token (second login step)

@end


// this class not use linkedin SDK, only pure web requests
@implementation InstagramLoginWrapper

@synthesize urlSchemePrefix = _urlSchemePrefix;

#pragma mark - AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    return YES;
}


- (void)applicationDidBecomeActive: (UIApplication *)application {
    
}

- (BOOL) application:(UIApplication *)app
             openURL:(NSURL *)url
             options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    
    [self parceCodeFromUrl: url.absoluteString];
    
    return YES;
}


#pragma mark - LoginWrapper

- (NSDictionary *) infoPlist_private {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"InstagramInfo" ofType:@"plist"];
    return [NSDictionary dictionaryWithContentsOfFile: path];
}

- (NSDictionary *) infoPlist_testing {
    
    NSMutableDictionary *mDict = [[self infoPlist_private] mutableCopy];
    [mDict removeObjectForKey:kCLIENT_ID];
    [mDict removeObjectForKey:kCLIENT_SECRET];
    [mDict removeObjectForKey:kREDIRECT_URL];
    
    return [NSDictionary dictionaryWithDictionary: mDict];
}


- (NSString *) siteName {
    return @"Instagram";
}


- (BOOL) nativeApplicationIsInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: @"instagram://"]];
}


- (NSString *) urlSchemePrefix {
    return _urlSchemePrefix ? _urlSchemePrefix : @"instagram";
}


- (void) setUrlSchemePrefix: (NSString *) prefix {
    
    _urlSchemePrefix = prefix ? [prefix copy]: @"instagram";
}


- (UIButton *)loginButton {
    
    static UIButton *loginButton;
    if(!loginButton) {
        
        loginButton = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        [loginButton setBackgroundImage: [UIImage imageNamed: @"instagram-button.png"] forState: UIControlStateNormal];
        [loginButton addTarget: self action: @selector(startLogin:) forControlEvents: UIControlEventTouchUpInside];
        
        self.loginBehavior = LoginBehaviorWeb; // UIWebView by default
    }
    
    return loginButton;
}


- (IBAction) startLogin:(id)sender {
    
    NSString *urlString = [self makeLoginUrl];
    
    if(self.loginBehavior == LoginBehaviorWeb) { // using UIWebView
        
        UIWindow *loginWindow = [[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
        loginWindow.windowLevel = UIWindowLevelAlert;
        
        UIWebView *webView = [[UIWebView alloc] initWithFrame: loginWindow.bounds];
        webView.delegate = self;
        [loginWindow addSubview: webView];
        webView.alpha = 0;
        
        [webView loadRequest: [NSURLRequest requestWithURL: [NSURL URLWithString: urlString]]];
        self.loginWindow = loginWindow;
        [self.loginWindow makeKeyAndVisible];
        
    } else if (self.loginBehavior == LoginBehaviorBrowser) { // safari + server
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: urlString] options:@{} completionHandler:nil];
        
    } else { // native app + server
        
        // not implemented (no documentation), this code open safari
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: urlString] options:@{} completionHandler:nil];
    }
}


- (NSString *) makeLoginUrl {
    
    NSDictionary *dict = [self infoPlist_private];
    
    NSString *clientId = [dict objectForKey:kCLIENT_ID];
    NSString *redirectUrl = [dict objectForKey:kREDIRECT_URL];
    // scopes 'basic' by default, url parameter format: scope=basic+public_content
    
    NSString *urlFormat = @"https://api.instagram.com/oauth/authorize/?client_id=%@&redirect_uri=%@&response_type=code";
    NSString *urlScring = [NSString stringWithFormat: urlFormat, clientId, redirectUrl];
    urlScring = [urlScring stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    return urlScring;
}


- (NSString *) makeTokenRequest {
    
    NSDictionary *dict = [self infoPlist_private];
    
    NSString *clientId = [dict objectForKey:kCLIENT_ID];
    NSString *clientSecret = [dict objectForKey:kCLIENT_SECRET];
    NSString *redirectUrl = [dict objectForKey:kREDIRECT_URL];
    
    NSString *urlFormat = @"grant_type=authorization_code&code=%@&redirect_uri=%@&client_id=%@&client_secret=%@";
    NSString *urlScring = [NSString stringWithFormat: urlFormat, self.code, redirectUrl, clientId, clientSecret];
    urlScring = [urlScring stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    return urlScring;
}



- (void) getUserInfo { // third step, download additional data (optional, try once)
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration: [NSURLSessionConfiguration defaultSessionConfiguration]];
    session.configuration.timeoutIntervalForRequest = 60.0f;
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.instagram.com/v1/users/self/?access_token=%@", self.token];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    request.HTTPMethod = @"GET";
    
    NSURLSessionDataTask *getDataTask =
    [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            LoginResult *loginResult = [LoginResult sharedResult];
            
            if (200 == [(NSHTTPURLResponse *)response statusCode] && [data length] && !error) {
                
                NSError *jsonError;
                id json;
                
                @try {
                    json = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingMutableContainers error: &jsonError];
                    
                } @finally {
                    
                    if(json && !jsonError) {
                        
                        // update loginResult from json here
                        NSLog(@"%@", json);
                    }
                }
            }
            
            [loginResult reloadAndNotify];
        });
    }];
    
    [getDataTask resume];
}


- (void) getToken { // second step, request access token
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration: [NSURLSessionConfiguration defaultSessionConfiguration]];
    session.configuration.timeoutIntervalForRequest = 60.0f;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: @"https://api.instagram.com/oauth/access_token"]];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [[self makeTokenRequest] dataUsingEncoding: NSUTF8StringEncoding];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSURLSessionDataTask *postDataTask =
    [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            if (200 == [(NSHTTPURLResponse *)response statusCode] && [data length] && !error) {
                
                NSError *jsonError;
                id json;
                
                @try {
                    json = [NSJSONSerialization JSONObjectWithData: data options:NSJSONReadingMutableContainers error: &jsonError];
                    self.token = json[@"access_token"];
                    
                } @finally {
                    
                    if(json && !jsonError && self.token && json[@"user"]) {
                        
                        LoginResult *loginResult = [LoginResult sharedResult];
                        loginResult.siteName = [self siteName];
                        
                        id user = json[@"user"];
                        loginResult.fullName = user[@"full_name"];
                        loginResult.nameComponent_1 = user[@"username"];
                        loginResult.accountIdentifier = user[@"id"];
                        loginResult.imageUrl = user[@"profile_picture"];
                        
                        loginResult.errorMessage = nil; // basic data exists and login success, try once download additional data
                        
                        // but...
                        BOOL emailNotAvailable = YES;
                        if(emailNotAvailable) {
                
                            [loginResult reloadAndNotify];
                
                        } else {
                            //[self getUserInfo];
                        }
                        
                    } else {
                        [self notifyError];
                    }
                }
            } else {
                [self notifyError];
            }
        });
    }];
    
    [postDataTask resume];
}


- (void) parceCodeFromUrl: (NSString *) absoluteUrlString { // first step, find authorization code
    
    // url format:  http://your-redirect-uri?code=CODE
    self.code = nil;
    
    if([absoluteUrlString rangeOfString:@"code"].location != NSNotFound) {
        
        @try {
            NSArray *components = [absoluteUrlString componentsSeparatedByString:@"?"]; // get paremeters
            self.code = [components[1] componentsSeparatedByString:@"="][1]; // get authorization code value
            
        } @catch (NSException *exception) {
            NSLog(@"Linkedin login fail %@", absoluteUrlString);
        }
    }
    
    if(self.code) {
        [self getToken];
    } else {
        [self notifyError];
    }
}

- (void) notifyError { // one code for all fails
    
    LoginResult *loginResult = [LoginResult sharedResult];
    
    loginResult.siteName = [self siteName];
    loginResult.errorMessage = LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE;
    
    [loginResult reloadAndNotify];
}



#pragma mark - UIWebViewDelegate


- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSDictionary *dict = [self infoPlist_private];
    NSString *redirectUrl = [dict objectForKey:kREDIRECT_URL];
    
    if([request.URL.absoluteString hasPrefix: redirectUrl]) {
        
        /* 1 this condition works after user enter login/password in web form,
         request to localhost must include authorization code as url parameter */
        
        ////////////////////////////////////////////////////
        [self parceCodeFromUrl: request.URL.absoluteString];
        ////////////////////////////////////////////////////
        
        // 2 remove all views animated
        webView.delegate = nil;
        self.loginWindow.userInteractionEnabled = NO;
        
        [UIView animateWithDuration: 0.15 animations:^{
            
            webView.alpha = 0;
            
        } completion:^(BOOL finished) {
            
            [webView removeFromSuperview];
            [self.loginWindow resignKeyWindow];
            self.loginWindow = nil;
        }];
        
        return NO;
    }
    
    return YES;
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [self notifyError];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    
    if(webView.alpha < 1) {
        [UIView animateWithDuration: 0.15 animations:^{
            webView.alpha = 1;
        }];
    }
}

@end
