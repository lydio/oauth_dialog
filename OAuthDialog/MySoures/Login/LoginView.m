//
//  LoginView.m
//  OAuthDialog
//
//  Created by apple on 9/30/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "LoginView.h"
#import "AppRoot.h"
#import "LoginCollectionViewCell.h"


@interface LoginView () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet UITextView *textView;

@end

@implementation LoginView

- (void) awakeFromNib {

    [super awakeFromNib];
    
    // init text
    NSString *fileName = @"AboutLogin";
    NSString *path = [[NSBundle mainBundle] pathForResource: fileName ofType: @"rtf"];
    
    if(path) {
        
        NSAttributedString *stringWithRTFAttributes =
        [[NSAttributedString alloc] initWithURL: [NSURL fileURLWithPath: path]
                                        options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType}
                             documentAttributes:nil
                                          error:nil];
        
        self.textView.attributedText =
        stringWithRTFAttributes ? stringWithRTFAttributes : [[NSAttributedString alloc] initWithString:@""];
    }
    
    // init items
    self.allButtons = @[];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}


- (void) setAllButtons:(NSArray<UIButton *> *)allButtons {
    
    _allButtons = allButtons;
    
    for(UIButton *button in _allButtons) {
        [self customizeButton: button];
    }
    
    [self.collectionView reloadData];
}


- (void) customizeButton: (UIButton *) button {

    // customize button here...
    // (may be create another class with custom styles - square, rect, color, font)
    
    CGSize size = self.collectionView.bounds.size;
    CGFloat width =   (300.0f * 365.0f) / size.width;
    CGFloat height =  (50.0f * 568.0f) / size.height;
    
    size = CGSizeMake(MIN(width, 350), MIN(height, 60));
    button.frame = CGRectMake(0, 0, size.width, size.height);
    //[button layoutSubviews];
}


#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate


- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section{
    
    return self.allButtons.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                     cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"LoginCollectionViewCell";
    static BOOL nibIsLoaded = NO;
    
    if(!nibIsLoaded) {
        
        UINib *nib = [UINib nibWithNibName: @"LoginCollectionViewCell" bundle: nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
        nibIsLoaded = YES;
    }
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier: identifier forIndexPath:indexPath];
    ((LoginCollectionViewCell *)cell).button = self.allButtons[indexPath.item];

    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.allButtons[indexPath.item].bounds.size;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    //CGSize size = self.collectionView.bounds.size;
    
    return 10;
}



- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    //CGSize size = self.collectionView.bounds.size;
   
    
    return 10;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    //CGSize size = self.collectionView.bounds.size;
    
    return UIEdgeInsetsMake(10, 0, 10, 0);
}



@end
