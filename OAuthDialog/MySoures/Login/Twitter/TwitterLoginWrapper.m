//
//  TwitterLoginWrapper.m
//  OAuthDialog
//
//  Created by apple on 10/3/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "TwitterLoginWrapper.h"
#import "LoginResult.h"

/* Fix Twitter warning 'DEBUG_INFORMATION_FORMAT should be set to dwarf-with-dsym for all configurations. ...':
 Build Settings -> Build Options -> Debug Information Format -> DWARF with dSYM file */
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>




@interface TwitterLoginWrapper ()

@end

@implementation TwitterLoginWrapper

#pragma mark - AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    /*
    Calling startWithConsumerKey:consumerSecret: will override any keys which were automatically configured. 
    Automatically configured keys live in your app’s Info.plist under the key Fabric.
     
    [[Twitter sharedInstance] startWithConsumerKey:@"your_key" consumerSecret:@"your_secret"];
    */
    
    [Fabric with:@[[Twitter class]]];

    return YES;
}


- (void)applicationDidBecomeActive: (UIApplication *)application {
    
}


- (BOOL) application:(UIApplication *)app
             openURL:(NSURL *)url
             options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options{
    
    BOOL result = [[Twitter sharedInstance] application:app openURL:url options:options];
    return result;
}


#pragma mark - LoginWrapper

- (NSDictionary *) infoPlist_testing {
    return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle mainBundle] pathForResource: @"TwitterInfo" ofType:@"plist"]];
}

- (NSString *) siteName {
    return @"Twitter";
}


- (BOOL) nativeApplicationIsInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString: @"twitter://"]];
}

- (NSString *) urlSchemePrefix {
    return @"twitter";
}


- (void) setLoginBehavior: (LoginBehaviorEnum) behavior {
    
    //TWTRLoginMethod ?
}


- (UIButton *)loginButton {

    static TWTRLogInButton *loginButton;
    if(!loginButton) {
        
        loginButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
            
            LoginResult *loginResult = [LoginResult sharedResult];
            
            loginResult.siteName = [self siteName];
            loginResult.errorMessage = (error || !session ? LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE : nil);
            
            if (!loginResult.errorMessage) {
                
                loginResult.fullName = [session userName];
                loginResult.accountIdentifier = [session userID];
                
                loginResult.errorMessage = nil; // login success
                
                [self getUserInfo]; // minimal login data are exists, try to load image url and email once
                
            } else {
                
                [loginResult reloadAndNotify];
            }
        }];
    }
    
    return loginButton;
}


- (void) getUserInfo {
    
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser]; // include right userId
    NSURLRequest *request = [client URLRequestWithMethod: @"GET"
                                                     URL: @"https://api.twitter.com/1.1/account/verify_credentials.json"
                                              parameters: @{@"include_email": @"true", @"skip_status": @"true"}
                                                   error: nil];
    
    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSError *jsonError;
        id json;
        
        @try {
            
            json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
            
        } @finally {
            
            LoginResult *loginResult = [LoginResult sharedResult];
            
            if(!connectionError && !jsonError && json) {
            
                loginResult.nameComponent_1 = [json objectForKey:@"screen_name"];
                loginResult.location = [json objectForKey:@"location"];
                loginResult.email = [json objectForKey: @"email"];
                loginResult.imageUrl = [json objectForKey:@"profile_image_url_https"];
            }
            
            [loginResult reloadAndNotify];
        }
    }];
}

/*
 - (void) getUserInfo {
 
     NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
     TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
 
     [client loadUserWithID: userID completion:^(TWTRUser *user, NSError *error) {
 
         LoginResult *loginResult = [LoginResult sharedResult];
 
         if(!error) {
 
             loginResult.imageUrl = user.profileImageURL;
             loginResult.nameComponent_1 = user.screenName;
 // !email not possible
 
         }
 
         [loginResult reloadAndNotify];
     }];
 }
*/

@end
