//
//  LoginResult.h
//  OAuthDialog
//
//  Created by apple on 10/5/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


#define LOGIN_RESULT_REGISTRATION_URL @"https://fakebackend.herokuapp.com/registration" //first request to heroku may be failed
//#define LOGIN_RESULT_REGISTRATION_URL  here your real url

#define LOGIN_RESULT_REGISTRATION_HEADERS_KEY @"Registration headers" //used to store response and parse or remove it later
#define LOGIN_RESULT_REGISTRATION_BODY_KEY @"Registration body"

#define LOGIN_RESULT_DEFAULT_ERROR_MESSAGE @"LoginResult is unusable"
#define LOGIN_RESULT_RECOMMENDED_ERROR_MESSAGE @"Login failed with error"
#define LOGIN_RESULT_RECOMMENDED_CANCEL_MESSAGE @"Login is cancelled"

#define LOGIN_RESULT_RELOAD_NOTIFICATION @"Reload LoginResult"
#define LOGIN_RESULT_DID_FINISH_LOAD_IMAGE_DATA_NOTIFICATION @"LoginResult did finish image data download"
#define LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION @"LoginResult did finish post result to server"



@interface LoginResult : NSObject 

+ (instancetype) sharedResult;
- (NSDictionary *) toDictionary;

- (void) reloadAndNotify; // LOGIN_RESULT_RELOAD_NOTIFICATION

- (void) loadImageDataAndNotify; //LOGIN_RESULT_DID_FINISH_LOAD_IMAGE_DATA_NOTIFICATION
- (void) postToServerAndNotify; // LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION


- (instancetype) init __attribute__((unavailable("init not available, call sharedInstance instead")));
+ (instancetype) new __attribute__((unavailable("new not available, call sharedInstance instead")));
- (instancetype) copy __attribute__((unavailable("copy not available, call sharedInstance instead")));


@property (nonatomic, copy) NSString *siteName; // unique site name

@property (nonatomic, copy) NSString *fullName; // grabbed user info
@property (nonatomic, copy) NSString *nameComponent_1;
@property (nonatomic, copy) NSString *nameComponent_2;

@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *birthday;
@property (nonatomic, copy) NSString *location;

@property (nonatomic, copy) NSData *imageData;
@property (nonatomic, copy) NSString *imageUrl;

@property (nonatomic, copy) NSString *accountIdentifier;
@property (nonatomic, copy) NSString *pageUrl;
@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSDictionary <NSString *, NSString *> *otherInfo; // reserved to store unsorted data, errors etc

@property (nonatomic, copy) NSString *phoneNumber; // manually entered info

@property (nonatomic, copy) NSString *errorMessage; // last response error

@property (nonatomic, copy) NSString *isRegistered; // flag that result was added to backend database

@end

