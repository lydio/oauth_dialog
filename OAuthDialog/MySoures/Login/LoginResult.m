//
//  LoginResult.m
//  OAuthDialog
//
//  Created by apple on 10/5/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "LoginResult.h"


#define LOGIN_RESULT_SITE_NAME @"siteName"
#define LOGIN_RESULT_FULL_NAME @"fullName"
#define LOGIN_RESULT_NAME_COMPONENT_1 @"nameComponent_1"
#define LOGIN_RESULT_NAME_COMPONENT_2 @"nameComponent_2"
#define LOGIN_RESULT_GENDER @"gender"
#define LOGIN_RESULT_BIRTHDAY @"birthday"
#define LOGIN_RESULT_LOCATION @"location"
#define LOGIN_RESULT_IMAGE_DATA @"imageData"
#define LOGIN_RESULT_IMAGE_URL @"imageUrl"
#define LOGIN_RESULT_ACCOUNT_IDENTIFIER @"accountIdentifier"
#define LOGIN_RESULT_PAGE_URL @"pageUrl"
#define LOGIN_RESULT_EMAIL @"email"
#define LOGIN_RESULT_OTHER_INFO @"otherInfo"
#define LOGIN_RESULT_PHONE_NUMBER @"phoneNumber"
#define LOGIN_RESULT_ERROR_MESSAGE @"errorMessage"
#define LOGIN_RESULT_IS_REGISTERED @"isRegistered"


@interface LoginResult() <NSCoding>
@end


@implementation LoginResult

+ (NSString *) savingPath {
    
    NSString *library =  [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) firstObject];
    NSString *path = [library stringByAppendingPathComponent: NSStringFromClass([self class])];
    
    return path;
}


+ (instancetype) sharedResult {
    
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        
        sharedInstance = [NSKeyedUnarchiver unarchiveObjectWithFile: [[self class] savingPath]];
        if(!sharedInstance) {
            sharedInstance = [[[self class] alloc] initWithCoder: [NSCoder new]];
        }
    });
    
    return sharedInstance;
}


- (void) reloadAndNotify {

    @synchronized (self) {
        
        [NSKeyedArchiver archiveRootObject: self toFile: [[self class] savingPath]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[NSNotificationCenter defaultCenter] postNotificationName: LOGIN_RESULT_RELOAD_NOTIFICATION object: self];
        });
    }
}


- (NSDictionary *) toDictionary {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];

    [dict setObject: self.siteName ? self.siteName : @"" forKey: LOGIN_RESULT_SITE_NAME];
    [dict setObject: self.fullName ? self.fullName : @"" forKey: LOGIN_RESULT_FULL_NAME];
    [dict setObject: self.nameComponent_1 ? self.nameComponent_1 : @"" forKey: LOGIN_RESULT_NAME_COMPONENT_1];
    [dict setObject: self.nameComponent_2 ? self.nameComponent_2 : @"" forKey: LOGIN_RESULT_NAME_COMPONENT_2];
    [dict setObject: self.gender ? self.gender : @"" forKey: LOGIN_RESULT_GENDER];
    [dict setObject: self.birthday ? self.birthday : @"" forKey: LOGIN_RESULT_BIRTHDAY];
    [dict setObject: self.location ? self.location : @"" forKey: LOGIN_RESULT_LOCATION];
    [dict setObject: self.imageData ? self.imageData : [NSData data] forKey: LOGIN_RESULT_IMAGE_DATA];
    [dict setObject: self.imageUrl ? self.imageUrl : @"" forKey: LOGIN_RESULT_IMAGE_URL];
    [dict setObject: self.accountIdentifier ? self.accountIdentifier : @"" forKey: LOGIN_RESULT_ACCOUNT_IDENTIFIER];
    [dict setObject: self.pageUrl ? self.pageUrl : @"" forKey: LOGIN_RESULT_PAGE_URL];
    [dict setObject: self.email ? self.email : @"" forKey: LOGIN_RESULT_EMAIL];
    [dict setObject: self.otherInfo ? self.otherInfo : [NSDictionary dictionary] forKey: LOGIN_RESULT_OTHER_INFO];
    [dict setObject: self.phoneNumber ? self.phoneNumber : @"" forKey: LOGIN_RESULT_PHONE_NUMBER];
    [dict setObject: self.errorMessage ? self.errorMessage : @"" forKey: LOGIN_RESULT_ERROR_MESSAGE];
    [dict setObject: self.isRegistered ? self.isRegistered : @"" forKey: LOGIN_RESULT_IS_REGISTERED];
    
    return [NSDictionary dictionaryWithDictionary: dict];
}


- (void) loadImageDataAndNotify {
    
    if(self.imageUrl) {
        
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^(void){
            
            NSURL *url = [NSURL URLWithString: self.imageUrl];
            NSData *newImageData = url ? [NSData dataWithContentsOfURL: url] : nil;
            UIImage *image = [UIImage imageWithData: newImageData];
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                if(image) { 
                    self.imageData = UIImagePNGRepresentation(image);
                    [NSKeyedArchiver archiveRootObject: self toFile: [[self class] savingPath]];
                }
                    
                [[NSNotificationCenter defaultCenter] postNotificationName:LOGIN_RESULT_DID_FINISH_LOAD_IMAGE_DATA_NOTIFICATION
                                                                    object:self];
            });
            
        });
    }
}


- (NSData *) serializedResult {
    
    NSMutableDictionary *dict = [[self toDictionary] mutableCopy];
    [dict removeObjectForKey: LOGIN_RESULT_IMAGE_DATA];
    
    if(self.imageData) {
        
        NSString *imageDataString = [self.imageData base64EncodedStringWithOptions: NSDataBase64Encoding64CharacterLineLength];
        [dict setObject: imageDataString forKey: LOGIN_RESULT_IMAGE_DATA];
    }
    
    NSString *string = [NSString stringWithFormat: @"%@", dict];
    NSData *data = [string dataUsingEncoding: NSUTF8StringEncoding];

    return data;
}


- (void) postToServerAndNotify {
    
    if(!self.errorMessage && !self.isRegistered) {
        
        dispatch_async(dispatch_get_main_queue(), ^(void) { //to synchronize with  - (void) loadImageDataAndNotify
        
            NSURLSession *session = [NSURLSession sessionWithConfiguration: [NSURLSessionConfiguration defaultSessionConfiguration]];
            session.configuration.timeoutIntervalForRequest = 60.0f;
        
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: LOGIN_RESULT_REGISTRATION_URL]];
            request.HTTPMethod = @"POST";
            request.HTTPBody = [self serializedResult];
            /* optionally : set name/password from keychains if user already registered */
        
            NSURLSessionDataTask *postDataTask =
            [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                
                    if (200 == [(NSHTTPURLResponse *)response statusCode]) {
                        
                        self.isRegistered = @"true";
                        
                        NSMutableDictionary *otherInfo = self.otherInfo != nil ? [self.otherInfo mutableCopy] : [NSMutableDictionary dictionary];
                        
                        NSString *dataString =
                        [[NSString alloc] initWithData: (data ? data : [NSData data]) encoding:NSUTF8StringEncoding];
                        [otherInfo setObject: dataString forKey: LOGIN_RESULT_REGISTRATION_BODY_KEY];
                        
                        NSDictionary *allHeaders =
                        [response isKindOfClass:[NSHTTPURLResponse class]] ? [(NSHTTPURLResponse *)response allHeaderFields] : @{};
                        NSData *jsonData =
                        [NSJSONSerialization dataWithJSONObject:allHeaders options:NSJSONWritingPrettyPrinted error:nil];
                        NSString *jsonString =
                        [[NSString alloc] initWithData: (jsonData ? jsonData : [NSData data]) encoding:NSUTF8StringEncoding];
                        [otherInfo setObject: jsonString forKey: LOGIN_RESULT_REGISTRATION_HEADERS_KEY];
                        
                        self.otherInfo = [NSDictionary dictionaryWithDictionary: otherInfo];
                        [NSKeyedArchiver archiveRootObject: self toFile: [[self class] savingPath]];
                        
                        /* optionally: get name/password from registration response and store it into keychains */
                    
                    } else {
                        /*
                         not set self.errorMessage here, all fields are correct, 
                         here is no field to store bad status,
                         failed registration request may be restarted from another class
                        */
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION
                                                                    object:self];
                });
            }];
        
            [postDataTask resume];
        });
    }
}


#pragma mark - NSCoding

- (instancetype) initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]) {
        
        if([aDecoder allowsKeyedCoding]) {
            
            self.siteName = [aDecoder decodeObjectForKey: LOGIN_RESULT_SITE_NAME];
            self.fullName = [aDecoder decodeObjectForKey: LOGIN_RESULT_FULL_NAME];
            self.nameComponent_1 = [aDecoder decodeObjectForKey: LOGIN_RESULT_NAME_COMPONENT_1];
            self.nameComponent_2 = [aDecoder decodeObjectForKey: LOGIN_RESULT_NAME_COMPONENT_2];
            self.gender = [aDecoder decodeObjectForKey: LOGIN_RESULT_GENDER];
            self.birthday = [aDecoder decodeObjectForKey: LOGIN_RESULT_BIRTHDAY];
            self.location = [aDecoder decodeObjectForKey: LOGIN_RESULT_LOCATION];
            self.imageData = [aDecoder decodeObjectForKey: LOGIN_RESULT_IMAGE_DATA];
            self.imageUrl = [aDecoder decodeObjectForKey: LOGIN_RESULT_IMAGE_URL];
            self.accountIdentifier = [aDecoder decodeObjectForKey: LOGIN_RESULT_ACCOUNT_IDENTIFIER];
            self.pageUrl = [aDecoder decodeObjectForKey: LOGIN_RESULT_PAGE_URL];
            self.email = [aDecoder decodeObjectForKey: LOGIN_RESULT_EMAIL];
            self.otherInfo = [aDecoder decodeObjectForKey: LOGIN_RESULT_OTHER_INFO];
            self.phoneNumber = [aDecoder decodeObjectForKey: LOGIN_RESULT_PHONE_NUMBER];
            self.errorMessage = [aDecoder decodeObjectForKey: LOGIN_RESULT_ERROR_MESSAGE];
            self.isRegistered = [aDecoder decodeObjectForKey: LOGIN_RESULT_IS_REGISTERED];
            
        } else {
            
            self.errorMessage = LOGIN_RESULT_DEFAULT_ERROR_MESSAGE;
        }
    }
    
    return self;
}


- (void) encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject: self.siteName forKey: LOGIN_RESULT_SITE_NAME];
    [aCoder encodeObject: self.fullName forKey: LOGIN_RESULT_FULL_NAME];
    [aCoder encodeObject: self.nameComponent_1 forKey: LOGIN_RESULT_NAME_COMPONENT_1];
    [aCoder encodeObject: self.nameComponent_2 forKey: LOGIN_RESULT_NAME_COMPONENT_2];
    [aCoder encodeObject: self.gender forKey: LOGIN_RESULT_GENDER];
    [aCoder encodeObject: self.birthday forKey:LOGIN_RESULT_BIRTHDAY];
    [aCoder encodeObject: self.location forKey: LOGIN_RESULT_LOCATION];
    [aCoder encodeObject: self.imageData forKey: LOGIN_RESULT_IMAGE_DATA];
    [aCoder encodeObject: self.imageUrl forKey: LOGIN_RESULT_IMAGE_URL];
    [aCoder encodeObject: self.accountIdentifier forKey: LOGIN_RESULT_ACCOUNT_IDENTIFIER];
    [aCoder encodeObject: self.pageUrl forKey: LOGIN_RESULT_PAGE_URL];
    [aCoder encodeObject: self.email forKey:LOGIN_RESULT_EMAIL];
    [aCoder encodeObject: self.otherInfo forKey: LOGIN_RESULT_OTHER_INFO];
    [aCoder encodeObject: self.phoneNumber forKey: LOGIN_RESULT_PHONE_NUMBER];
    [aCoder encodeObject: self.errorMessage forKey: LOGIN_RESULT_ERROR_MESSAGE];
    [aCoder encodeObject: self.isRegistered forKey: LOGIN_RESULT_IS_REGISTERED];
}



@end
