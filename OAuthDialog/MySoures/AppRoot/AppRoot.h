//
//  AppRoot.h
//  OAuthDialog
//
//  Created by apple on 9/30/16.
//  Copyright © 2016 apple. All rights reserved.
//
// parent class for AppDelegate, used to make reusable social functionality code


/*
 Fake apps
 
 Facebook application
    YHTe­s­t­A­p­p­l­i­c­a­tion
    AppId - 1851447355092934
    Secret - 109a5aecd6dee3739173e981db08c76a
 Google
    yhte­s­t­a­p­p­l­i­c­a­tion
    AppId - 1:1044001112811:ios:6998f53966096555
    Secret - 
 Twitter
    yhtestapplication
    Consumer Key -zoHCmINIxUOnUIreFTFB2bWkx
    Consumer Secret -bhv5REXP876I17SbcVo2T6OZQff4trt7gLqgORpXQcg71yNIaP
    Run script build phase - 
    ${PODS_ROOT}/Fabric/run 7bb53b2b367b7ce74cc7ceb72e716b21a1a2600c e8f5a08103514c6645c5a25cada100296fd0b5ffe8b7777719b8d61782c9ff3f
 LinkedIn
    ClientID - 78soitxe6mv8zz
    ClientSecret - q66MI3UxlJz5oueb
    ApplicationID - 4295894
 Instagram
    yhte­s­t­a­p­p­l­i­c­a­tion
    CLIENT ID	9ec98e067aee46d6a4da8a1d0a475d44
    Client Secret cd68b10a94774badb15e1f88564a80ca
 
    
 
 Don't forget also add Project-> Capabilities -> Keychain Sharing -> On
*/


#import <UIKit/UIKit.h>


typedef enum NSUInteger { LoginBehaviorWeb, LoginBehaviorBrowser, LoginBehaviorNative } LoginBehaviorEnum;

@protocol LoginWrapper <NSObject>

@required

/* all settings must be added manually to Info.plist, 
 no any way to resolve it at runtime,
 use tests to check that settings is correct 
 Method settingsToTest must return small snippet of Info.plist with your real configuration parameters
*/
- (NSDictionary *) infoPlist_testing;

- (NSString *) siteName; // unique string key like 'Facebook', 'Google' etc

- (UIControl *) loginButton;

/* About behavior
 Use URL Schemes if you need to use native app or browser!
 See Got-Ya info.plist and tutorial https://dev.twitter.com/cards/mobile/url-schemes
 */
- (void) setLoginBehavior: (LoginBehaviorEnum) behavior;

- (BOOL) nativeApplicationIsInstalled;

- (NSString *) urlSchemePrefix; /*see optional setter below*/

@optional
/*
 Some wrappers (LinkedIn, Instagram ...) use pure web api (not iOS SDK),
 and for redirect is used default application url scheme, now is 'oauthdialog://'
 Is needed to set this default url scheme only for one (selected) wrapper and
 reset to nil for others
*/
- (void) setUrlSchemePrefix: (NSString *) prefix;

@end



@interface AppRoot : UIResponder <UIApplicationDelegate>

+ (instancetype) sharedInstance; // return AppDelegate existed instance
- (void) tests;


- (void) presentProfileViewController;
- (void) dismissProfileViewController;


// overrided AppDelegate methods
         - (BOOL) application: (UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions __attribute__((objc_requires_super));

- (void) applicationDidBecomeActive:(UIApplication *)application __attribute__((objc_requires_super));

- (BOOL) application: (UIApplication *)app
             openURL: (NSURL *)url
             options: (NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options __attribute__((objc_requires_super));

@end
