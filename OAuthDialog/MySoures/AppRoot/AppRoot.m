//
//  AppRoot.m
//  OAuthDialog
//
//  Created by apple on 9/30/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "AppRoot.h"
#import <objc/runtime.h>
#import <UICKeyChainStore.h>
#import "Reachability.h"

#import "../../../Pods/FBSDKCoreKit/FBSDKCoreKit/FBSDKCoreKit/Internal/FBSDKInternalUtility.h"

#import "ProfileView.h"
#import "ProfileViewController.h"
#import "LoginView.h"
#import "LoginViewController.h"
#import "LoginResult.h"

#import "FacebookLoginWrapper.h"
#import "GoogleLoginWrapper.h"
#import "TwitterLoginWrapper.h"
#import "LinkedinLoginWrapper.h"
#import "InstagramLoginWrapper.h"


@interface AppRoot ()

@property (nonatomic, strong) NSDictionary <NSString *, id <LoginWrapper, UIApplicationDelegate>> *loginWrappers;
@property (nonatomic, readonly) NSArray <UIButton *> *loginButtons;
@property (nonatomic) BOOL userInteractionEnabled;

@end


@implementation AppRoot

+ (instancetype) sharedInstance {
    
    static AppRoot *instance = nil;
    
    if(!instance) {
        
        UIApplication *app = [UIApplication sharedApplication];
        
        if([app.delegate isKindOfClass: [AppRoot class]]) {
            
            instance = (AppRoot *) app.delegate;
        
        } else {
            
            if(!app.delegate) {
                @throw [NSException exceptionWithName:@"AppRoot will be initialised later" reason:nil userInfo:nil];
            } else {
                @throw [NSException exceptionWithName:@"AppRoot must be AppDelegate superclass" reason:nil userInfo:nil];
            }
        }
    }
    
    return instance;
}


- (instancetype) init {
    
    if(self = [super init]) {
        
        // 0 local vars
        _userInteractionEnabled = YES;
        
        // 1 init wrappers
        NSMutableDictionary *loginWrappers = [NSMutableDictionary dictionary];
        
        FacebookLoginWrapper *facebookLoginWrapper = [FacebookLoginWrapper new];
        [loginWrappers setObject: facebookLoginWrapper forKey: [facebookLoginWrapper siteName]];
        
        GoogleLoginWrapper *googleLoginWrapper = [GoogleLoginWrapper new];
        [loginWrappers setObject: googleLoginWrapper forKey: [googleLoginWrapper siteName]];
        
        TwitterLoginWrapper *twitterLoginWrapper = [TwitterLoginWrapper new];
        [loginWrappers setObject: twitterLoginWrapper forKey: [twitterLoginWrapper siteName]];
        
        LinkedinLoginWrapper *linkedinLoginWrapper = [LinkedinLoginWrapper new];
        [loginWrappers setObject: linkedinLoginWrapper forKey: [linkedinLoginWrapper siteName]];
        
        InstagramLoginWrapper *instagramLoginWrapper = [InstagramLoginWrapper new];
        [loginWrappers setObject: instagramLoginWrapper forKey: [instagramLoginWrapper siteName]];

        self.loginWrappers = [NSDictionary dictionaryWithDictionary: loginWrappers];
        
        // 2 set login behavior
        for(id<LoginWrapper> wrapper in self.loginWrappers.allValues) {
            
            if([wrapper nativeApplicationIsInstalled]) {
                [wrapper setLoginBehavior: LoginBehaviorNative];
            } else {
                [wrapper setLoginBehavior: LoginBehaviorBrowser];
            }
            // LoginBehaviorWeb by default
        }
    }
    
    return self;
}


- (NSString *) defaultUrlScheme {
    
    static NSString *scheme = nil;
    
    if(!scheme) {
        
        @try {
            // уточнить в тестах как то что выше нулувой - схемы не создавать, только под ней
            NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
            NSArray *urlTypes = [infoPlist objectForKey:@"CFBundleURLTypes"];
            NSArray *schemes = [(NSDictionary *)urlTypes.firstObject objectForKey:@"CFBundleURLSchemes"];
            scheme = schemes.firstObject;
            
        } @finally {
            if(!scheme) @throw [NSException exceptionWithName:@"Can't find default url scheme for login" reason:nil userInfo:nil];
        }
    }
    
    return scheme;
}


- (BOOL) infoPlist: (NSObject <NSCopying>*) wrongObject compareWith: (NSObject <NSCopying>*) correctObject {
    
    __strong NSObject *o1 = [correctObject copy];
    __strong NSObject *o2 = [wrongObject copy];

    if(!o1 || !o2) return NO;
    
    if(([o1 isKindOfClass: [NSString class]] && [o2 isKindOfClass: [NSString class]]) ||
       ([o1 isKindOfClass: [NSNumber class]] && [o2 isKindOfClass: [NSNumber class]]) ||
       ([o1 isKindOfClass: [NSData class]] && [o2 isKindOfClass: [NSData class]])) {
        
        return  [o1 isEqual: o2];
    }
    
    if([o1 isKindOfClass: [NSDictionary class]] && [o2 isKindOfClass: [NSDictionary class]]) {
        
        NSDictionary *d1 = (NSDictionary *)o1;
        NSDictionary *d2 = (NSDictionary *)o2;
        BOOL result = YES;
        
        for(NSString *key in d1.allKeys) {
            
            NSObject *v1 = [d1 valueForKey: key];
            NSObject *v2 = [d2 valueForKey: key];
            result = v2 ? (result && [self infoPlist: (NSObject <NSCopying> *)v2 compareWith: (NSObject <NSCopying> *)v1]) : NO;
        }
        
        return result;
    }
    
    if([o1 isKindOfClass:[NSArray class]] && [o2 isKindOfClass:[NSArray class]]) {
        
        NSArray *a1 = (NSArray *)o1;
        NSArray *a2 = (NSArray *)o2;
        
        for(NSObject * i1 in a1) {
            for(NSObject *i2 in a2) {
                
                if([self infoPlist: (NSObject <NSCopying> *)i2
                       compareWith: (NSObject <NSCopying> *)i1]) {
                    return YES;
                    break;
                }
            }
        }
    }
    
    return NO;
}


- (void) tests {
    
    // 0 Class AppDelegate must be inherited from AppRoot class
    NSAssert([UIApplication sharedApplication].delegate == self, @"Test 0: AppDelegate is not a kind of AppRoot class");
    
    // 1 Check that we have UrlSheme for launch this app from Safari
    NSDictionary *infoPlist = [[NSBundle mainBundle] infoDictionary];
    
    NSArray *urlTypes = [infoPlist objectForKey:@"CFBundleURLTypes"];
    BOOL schemeExists = NO;
    
    for(NSDictionary *dict in urlTypes) {
        NSArray *schemes = [dict objectForKey:@"CFBundleURLSchemes"];
        if([schemes count] == 1 &&
           [schemes[0] isKindOfClass:[NSString class]] &&
           [schemes[0] isEqual: @"oauthdialog"]) { // 'oauthdialog' is not app name, it just url sheme 'oauthdialog://' to open app from safari
            schemeExists = [[dict objectForKey:@"CFBundleURLName"] isEqual:  [infoPlist objectForKey:@"CFBundleIdentifier"]];
            break;
        }
    }
    NSAssert(schemeExists, @"Test 1: Set correct url scheme to your application (see CFBundleURLName setting)");
    
    // 2 Assess to photos
    NSAssert([infoPlist objectForKey:@"NSPhotoLibraryUsageDescription"] &&
             [infoPlist objectForKey:@"NSCameraUsageDescription"], @"Test 2.1. To change profile photo access to user photo library is required");
    
    
    
    // 7 File <PROJECT_NAME>.entitlements exists, keychain sharing is ON (facebook need it)
    UICKeyChainStore *keychainStore = [UICKeyChainStore keyChainStore];
    keychainStore[@"password"] = @"abcd1234";
    NSAssert(keychainStore[@"password"], @"Test 7: Some APIs need keychain sharing,  set Project-> Capabilities -> Keychain Sharing -> On");
    
    // 8 Test wrappers functional
    NSMutableString *sites = [[NSMutableString alloc] initWithString:@"Sites:"];
    for (id <LoginWrapper, UIApplicationDelegate> wrapper in self.loginWrappers.allValues) {
        
        // Site name will be used as unique key in application
        NSString *siteName = [wrapper siteName];
        NSAssert([siteName length], @"Test 8.1: Site name must not be nil");
        NSAssert([sites rangeOfString: siteName].location == NSNotFound, @"Test 8: Site name must be unique");
        [sites appendString: siteName];
        
        //LoginButtons must not be nil (check it here, because attribute __Nonnull in methods generate many warnings)
        NSAssert([wrapper loginButton], @"Test 8.2: LoginButton must be initialized");
        
        // Check all settings in Info.plist for each sdk (except Google)
        
        NSDictionary *requiredPlist = [wrapper infoPlist_testing];
        NSAssert([requiredPlist count], @"Test 8.3: No values to testing Info.plist");
        NSAssert([self infoPlist: infoPlist compareWith: requiredPlist], @"Check your Info.plist");
    }
    
    // 9 Check network connection and clear simulator
    NSAssert(![[NSUserDefaults standardUserDefaults] objectForKey:@"kFirstLaunch"], @"Test 9.1: Clear simulator to continue");
    [[NSUserDefaults standardUserDefaults] setObject: @"*" forKey: @"kFirstLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSAssert([[Reachability reachabilityForInternetConnection] currentReachabilityStatus], @"Test 9.2: No internet connection");
    
    // 10 Login and Registration(send data to app backend server, get code 200 and password)
    __block NSInteger n = 0;
    void (^runLogin)() = ^(){
        
        if( n < self.loginWrappers.count) {
            UIControl *loginButton = [self.loginWrappers.allValues[n] loginButton]; // LoginBehaviorWeb by default
            [loginButton sendActionsForControlEvents: UIControlEventTouchUpInside];
        }
    };
    
    [[NSNotificationCenter defaultCenter] addObserverForName: LOGIN_RESULT_RELOAD_NOTIFICATION object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        LoginResult *result = note.object;
        NSAssert(!result.errorMessage, @"Test 10.1: Check what requested permissions are correct, InfoAdditions.plist settings, entered name/password, internet is connected");
        
        [[NSNotificationCenter defaultCenter] removeObserver: self name: LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION object: nil];
        [[NSNotificationCenter defaultCenter] addObserverForName: LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION object: nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            
            LoginResult *result = note.object;
            NSAssert(result.isRegistered, @"Test 10.2: Can not register user, check connection, request configuration and server logic");
            
            if(n >= self.loginWrappers.count) {
                
                NSLog(@"\n==============\nTest complete!\n==============\n");
                exit(0);
                
            } else {
                
                result.errorMessage = LOGIN_RESULT_DEFAULT_ERROR_MESSAGE;
                result.isRegistered = nil;
                
                n++;
                runLogin();
            }
        }];
        
        [result postToServerAndNotify];
    }];
    
    runLogin();
}


- (void) updateLoginByNotification: (NSNotification *) notification {
    
    self.userInteractionEnabled = YES; // 'NO' was setted then login button did press (see loginButtons property)
    
    LoginResult *loginResult = (LoginResult *)notification.object;
    
    if(!loginResult.errorMessage) {
        
        [self dismissLoginViewController];
        
    } else {
        
        [self presentLoginViewController];
    }
}


- (void) presentLoginViewController {

    UIViewController *vc = self.window.rootViewController.presentedViewController;
    if(!vc || ![vc isKindOfClass: [LoginViewController class]]) {
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(updateLoginByNotification:)
                                                     name: LOGIN_RESULT_RELOAD_NOTIFICATION
                                                   object: [LoginResult sharedResult]];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LoginStoryboard" bundle:nil];
        LoginViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        loginViewController.loginView.allButtons = self.loginButtons;

        [self.window.rootViewController presentViewController: loginViewController animated: YES completion:^{
            // здесь проверить есть ли ошибка и какая и как то отобразить
        }];
    }
}


- (void) dismissLoginViewController {

    UIViewController *vc = self.window.rootViewController.presentedViewController;
    if(vc && [vc isKindOfClass: [LoginViewController class]]) {
        
        [[NSNotificationCenter defaultCenter] removeObserver: self
                                                        name: LOGIN_RESULT_RELOAD_NOTIFICATION
                                                      object: [LoginResult sharedResult]];
        
        [self.window.rootViewController dismissViewControllerAnimated: YES completion:^{
            
            // Отправляем данные на сервер, регистрируем пользователя (делаем сразу же, так как данные только что получены, а значит интернет работает, отправляем без фото, чтоб быстрее)
            [self createUserAccount_once];
        }];
    }
}


- (void) presentProfileViewController {
    
    UIViewController *vc = self.window.rootViewController.presentedViewController;
    if(!vc || ![vc isKindOfClass: [ProfileViewController class]]) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ProfileStoryboard" bundle:nil];
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"ProfileNavigationController"];
        
        ProfileViewController *profileViewController = navigationController.viewControllers[0];
        LoginResult *loginResult = [LoginResult sharedResult];
        profileViewController.profileView.userName = loginResult.fullName;
        profileViewController.profileView.email = loginResult.email;
        
        [self.window.rootViewController presentViewController: navigationController animated: YES completion:^{
            
            // загрузка картинки если еще не закачали (однократно, если загрузка зафейлилась втыкаем placeholder image)
            if([loginResult.imageData length]) {
                
                profileViewController.profileView.profileImageData = loginResult.imageData;
            
            } else if(loginResult.imageUrl) {
                
                self.userInteractionEnabled = NO;
                
                [[NSNotificationCenter defaultCenter] addObserverForName: LOGIN_RESULT_DID_FINISH_LOAD_IMAGE_DATA_NOTIFICATION object: nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
                    
                    [[NSNotificationCenter defaultCenter] removeObserver: self name: LOGIN_RESULT_DID_FINISH_LOAD_IMAGE_DATA_NOTIFICATION object: nil];
                    
                    LoginResult *loginResult = note.object;
                    if([loginResult.imageData length]) {
                        profileViewController.profileView.profileImageData = loginResult.imageData;
                    }
                    
                    self.userInteractionEnabled = YES;
                }];
                
                [loginResult loadImageDataAndNotify];
            }
            
        }];
    }
}


- (void) dismissProfileViewController {

    UIViewController *vc = self.window.rootViewController.presentedViewController;
    if(vc &&
       [vc isKindOfClass: [UINavigationController class]] &&
       [[(UINavigationController *)vc viewControllers][0] isKindOfClass: [ProfileViewController class]]) {
        
        /* 
         здесь вычитать из представления данные и отправить изменения на сервер,
         отправлять однократно, с блокировкой UI, 
         если код 200 - переписать LoginResult,
         если другое - откатить UI форму назад + сообщение об ошибке
        */
        [self.window.rootViewController dismissViewControllerAnimated: YES completion:^{
            
        }];
    }
}



- (void) createUserAccount_once {
    
    [[NSNotificationCenter defaultCenter] addObserverForName: LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION object: nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        
        LoginResult *loginResult = note.object;
        if(loginResult.isRegistered) {
            
            self.userInteractionEnabled = YES;
            [[NSNotificationCenter defaultCenter] removeObserver: self name: LOGIN_RESULT_DID_FINISH_POST_REQUEST_NOTIFICATION object: nil];
            
            // показываем профайл (необязательно), чтоб юзер увидел, что его после всех действий таки успешно порегали
            [self presentProfileViewController];
            
        } else {
        
            self.userInteractionEnabled = NO;
            [loginResult postToServerAndNotify];
        }
        
    }];
    
    LoginResult *loginResult = [LoginResult sharedResult];
    if(!loginResult.errorMessage && !loginResult.isRegistered) {
        
        // в принципе здесь уже можно не проверять ни первое, ни второе условие
        // отсутствие errorMessage означает, что oauth прошел успешно, данные заграбили
        // отстутсвие isRegistered означает, что аккаунт на бэкэнде не создан - будет вертеть вертушкой пока не создаст
        
        self.userInteractionEnabled = NO;
        [loginResult postToServerAndNotify];
    
    } else {
        
        @throw [NSException exceptionWithName: @"May be code was edited uncerrectly" reason: nil userInfo: nil];
    }
}


- (void) setUserInteractionEnabled: (BOOL) enabled { // block all user activity
    
    if(_userInteractionEnabled != enabled) {
        
        _userInteractionEnabled = enabled;
        
        if(!_userInteractionEnabled) {
            
            UIView *tintView = [[UIView alloc] initWithFrame: self.window.bounds];
            tintView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            tintView.backgroundColor = [UIColor colorWithWhite: 0.0f alpha: 0.25f];
            tintView.tag = 1976;
            
            UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            activityIndicatorView.center = CGPointMake(tintView.bounds.size.width/2, tintView.bounds.size.height/2);
            
            [tintView addSubview: activityIndicatorView];
            [self.window addSubview: tintView];
            
            self.window.userInteractionEnabled = NO;
            [activityIndicatorView startAnimating];
            
        } else {
            
            UIView *tintView = [self.window viewWithTag: 1976];
            if(tintView) {
                [(UIActivityIndicatorView *)tintView.subviews[0] stopAnimating];
                [tintView removeFromSuperview];
                
                self.window.userInteractionEnabled = YES;
            }
        }
    }
}


- (void) loginButtonDidPress:(UIControl *) button {
    
    for(id <LoginWrapper, UIApplicationDelegate> wrapper in self.loginWrappers.allValues) {
        
        if([wrapper respondsToSelector: @selector(setUrlSchemePrefix:)]) {
        
            NSString *defaultUrlScheme = (button == [wrapper loginButton] ? [self defaultUrlScheme] : nil);
            [wrapper performSelector: @selector(setUrlSchemePrefix:) withObject: defaultUrlScheme];
        }
    }
}


- (NSArray <UIButton *> *) loginButtons {
    
    NSMutableArray *allButtons = [NSMutableArray array];
    NSArray *allKeys = [self.loginWrappers allKeys];
    allKeys = [allKeys sortedArrayUsingDescriptors:[NSArray arrayWithObject: [NSSortDescriptor sortDescriptorWithKey:@"self" ascending: YES]]];
    
    for(NSInteger i = 0; i < allKeys.count; i++) {
        
        NSString *key = allKeys[i];
        UIControl *button = [self.loginWrappers[key] loginButton];
        [button addTarget:self action: @selector(setUserInteractionEnabled:) forControlEvents: UIControlEventTouchUpInside];
        [button addTarget:self action: @selector(loginButtonDidPress:) forControlEvents: UIControlEventTouchUpInside];
        
        [allButtons addObject: button];
    }
    
    return [NSArray arrayWithArray: allButtons];
}



#pragma mark - UIApplicationDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSString *documents =  [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"AppDirectory: \n%@\n", [documents stringByDeletingLastPathComponent]);
    NSLog(@"Launch Options: \n%@\n", launchOptions);
    
    if([LoginResult sharedResult].errorMessage) {
        
        for(id<UIApplicationDelegate> wrapper in self.loginWrappers.allValues) {
            [wrapper application: application didFinishLaunchingWithOptions: launchOptions];
        }
    }
    
    ///////////////
    //[self tests];
    ///////////////
    
    return YES;
}



- (void) applicationDidBecomeActive:(UIApplication *)application {
    
    // при логине через вебвьюшку это не появляется повторно (тк приложение не падало в фон)
    
    if([LoginResult sharedResult].errorMessage) {
        
        [self presentLoginViewController];
        
        for(id<UIApplicationDelegate> wrapper in self.loginWrappers.allValues) {
            [wrapper applicationDidBecomeActive: application];
        }
        
    } else {
        
        [self dismissLoginViewController];
    }
}


- (BOOL) application:(UIApplication *)app
             openURL:(NSURL *)url
             options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    
    // при логине через вебвьюшку этот метод вообще не срабатывает (у фэйсбука точно)
    //NSLog(@"Calling Application Bundle ID: %@", options[@"UIApplicationOpenURLOptionsSourceApplicationKey"]);
    //NSLog(@"URL : %@", url);
    //NSLog(@"URL scheme:%@", [url scheme]);
    //NSLog(@"URL query: %@", [url query]);
    
    for(NSString *key in self.loginWrappers.allKeys) {
            
        id<LoginWrapper, UIApplicationDelegate> wrapper = self.loginWrappers[key];
            
        if([[url scheme] hasPrefix: [wrapper urlSchemePrefix]]) {
            
            if([wrapper respondsToSelector: @selector(setUrlSchemePrefix:)]) {
                [wrapper performSelector: @selector(setUrlSchemePrefix:) withObject: nil];
            }
                
            return [wrapper application: app openURL: url options: options];
        }
    }

    return NO;
}



@end






