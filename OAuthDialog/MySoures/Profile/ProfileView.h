//
//  ProfileView.h
//  OAuthDialog
//
//  Created by apple on 10/11/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProfileView : UIView

@property (nonatomic, copy) NSData *profileImageData; // animated
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *email;
// ...add more properties


@end


