//
//  ProfileView.m
//  OAuthDialog
//
//  Created by apple on 10/11/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import "ProfileView.h"


@interface ProfileCircleImageButtonView : UIView

@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic) CGFloat strokeWidth;
@property (nonatomic, readonly) UIButton *button;
@property (nonatomic, readonly) UITapGestureRecognizer *backgroundTapRecognizer;

@end


@interface ProfileView () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet ProfileCircleImageButtonView *profilePhotoView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (strong, nonatomic) UIImage *placeholderImage;
@property (strong, nonatomic) NSData *profileImageData_private;


@end


@implementation ProfileView {
    
    UITextField *selectedTextField;
}


- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}



- (void) awakeFromNib {
    [super awakeFromNib];
    
    NSArray *controls = @[self.profilePhotoView,
                          self.usernameTextField,
                          self.emailTextField];
    
    for(UIView *c in controls) {
        
        [c removeFromSuperview];
        [self.scrollView addSubview: c];
        
        if([c isKindOfClass: [UITextField class]]) {
            [(UITextField *) c setDelegate: self];
        }
    }
    
    self.scrollView.scrollEnabled = NO;
    self.scrollView.pagingEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidChangeFrame:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(backgroundTap)];
    [self addGestureRecognizer: tapGR];
    
    self.profilePhotoView.strokeWidth = 1.0f;
    self.profilePhotoView.strokeColor = [UIColor blackColor];
    
    self.placeholderImage = [UIImage imageNamed:@"ProfileImage.png"];
    [self.profilePhotoView.button setImage: self.placeholderImage forState: UIControlStateNormal];
}



- (void) keyboardWillHide: (NSNotification *) note {
    
    [self.scrollView setContentOffset: CGPointMake(0, 0) animated: YES];
}

- (void) keyboardDidChangeFrame: (NSNotification *) note {
    
    if(selectedTextField) { // всегда YES
        
        NSValue *frameEnd = [[note userInfo] valueForKey:@"UIKeyboardFrameEndUserInfoKey"];
        CGRect keyboardFrame = [frameEnd CGRectValue];
        
        CGFloat y1 = keyboardFrame.origin.y;
        CGFloat y2 = selectedTextField.frame.origin.y + selectedTextField.frame.size.height + 10;
        [self.scrollView setContentOffset: CGPointMake(0, y2 - y1) animated: YES];
    }
}


- (void) backgroundTap {
    
    [self endEditing: YES];
}

- (void) orientationDidChange: (id) sender {
    
    [self layoutSubviews];
}


- (void) layoutSubviews {
    
    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
    
    if(Orientation==UIDeviceOrientationLandscapeLeft || Orientation==UIDeviceOrientationLandscapeRight) {
        [self layoutSubviewsHorisontally];
    } else if(Orientation==UIDeviceOrientationPortrait || Orientation == UIDeviceOrientationPortraitUpsideDown) {
        [self layoutSubviewsVertically];
    } else {
        [self layoutSubviewsVertically]; // this possible after [application openUrl:  ...]
    }
}

- (void) layoutSubviewsVertically {

    CGSize size = self.bounds.size;
    
    self.scrollView.frame = CGRectMake(0, 0, size.width, size.height);
    self.scrollView.contentSize = size;
    
    CGFloat imageSide =  MAX(0.6f*MIN(size.width, size.height/2), 240.0f);
    CGFloat textHeight = imageSide/8;
    
    self.profilePhotoView.frame = CGRectMake(0, 0, imageSide, imageSide);
    self.usernameTextField.frame = CGRectMake(0, 0, imageSide, textHeight);
    self.emailTextField.frame = CGRectMake(0, 0, imageSide, textHeight);
    
    CGPoint center = self.scrollView.center;
    
    self.profilePhotoView.center = CGPointMake(center.x, center.y / 2);
    self.usernameTextField.center = CGPointMake(center.x, self.profilePhotoView.center.y + imageSide/2 + textHeight/2 + 20);
    self.emailTextField.center = CGPointMake(center.x,  self.usernameTextField.center.y + textHeight/2 + textHeight/2 + 10);
    
    self.scrollView.contentOffset = CGPointMake(0, 0);
}

- (void) layoutSubviewsHorisontally {
    
    CGSize size = self.bounds.size;
    
    self.scrollView.frame = CGRectMake(0, 0, size.width, size.height);
    self.scrollView.contentSize = size;
    
    CGFloat imageSide =  MAX(0.6f*MIN(size.width, size.height/2), 240.0f);
    CGFloat textHeight = imageSide/8;
    
    self.profilePhotoView.frame = CGRectMake(0, 0, imageSide, imageSide);
    self.usernameTextField.frame = CGRectMake(0, 0, imageSide, textHeight);
    self.emailTextField.frame = CGRectMake(0, 0, imageSide, textHeight);
    
    CGPoint center = self.scrollView.center;
    
    self.profilePhotoView.center = CGPointMake(center.x/2, center.y);
    self.usernameTextField.center = CGPointMake(self.profilePhotoView.center.x + imageSide + 20, center.y - textHeight/2 - 5);
    self.emailTextField.center = CGPointMake(self.usernameTextField.center.x,  center.y + textHeight/2 + 5);
    
    self.scrollView.contentOffset = CGPointMake(0, 0);
}


#pragma mark - login properties

- (NSData *) profileImageData {
    
    return self.profileImageData_private;
}

- (void) setProfileImageData: (NSData *)profileImageData {
    
    self.profileImageData_private = [profileImageData copy];
    UIImage *image = [UIImage imageWithData: _profileImageData_private];
    
    if([_profileImageData_private length] && image) {
        
        [UIView animateWithDuration: 0.125 animations:^{
            self.profilePhotoView.userInteractionEnabled = NO;
            self.profilePhotoView.alpha = 0;
            
        } completion:^(BOOL finished) {
            
            [self.profilePhotoView.button setImage: image forState:UIControlStateNormal];
            
            [UIView animateWithDuration: 0.125 animations:^{
                
                self.profilePhotoView.alpha = 1;
                self.profilePhotoView.userInteractionEnabled = YES;
            }];
        }];
    }
}

- (NSString *) userName {
    return self.usernameTextField.text; // return @"" instead nil
}

- (void) setUserName:(NSString *)userName {
    self.usernameTextField.text = [userName copy];
}

- (NSString *)email {
    return self.emailTextField.text;
}

- (void) setEmail:(NSString *)email {
    self.emailTextField.text = [email copy];
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    selectedTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    selectedTextField = nil;
}

@end



//
@implementation ProfileCircleImageButtonView

@synthesize button = _button;
@synthesize strokeColor = _strokeColor;
@synthesize strokeWidth = _strokeWidth;
@synthesize backgroundTapRecognizer = _backgroundTapRecognizer;

- (void) dealloc {
    if (_backgroundTapRecognizer) [self removeGestureRecognizer: _backgroundTapRecognizer];
}

- (id) initWithFrame:(CGRect)frame {

    if (self = [super initWithFrame:frame]) {
        [self initCode];
    }
    
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder {
    
    if (self = [super initWithCoder:aDecoder]) {
        [self initCode];
    }
    
    return self;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame: frame];
    
    [self updateFrame];
    
}

- (void) updateConstraints {
    [super updateConstraints];
    
    [self updateFrame];
}

- (void) initCode {
    
    self.backgroundColor = [UIColor clearColor];
    _button = [UIButton buttonWithType: UIButtonTypeCustom];
    _strokeColor = [UIColor clearColor];
    _strokeWidth = 0.0;
    [self updateFrame];
    
    [self addSubview: _button];
}

- (void) setStrokeColor:(UIColor *)strokeColor {
    _strokeColor = strokeColor != nil ? strokeColor : [UIColor clearColor];
    [self updateFrame];
}

- (void) setStrokeWidth:(CGFloat)strokeWidth {
    _strokeWidth = strokeWidth > 0.0 ? strokeWidth : 0.0;
    [self updateFrame];
}

- (void) updateFrame {
    
    CGFloat side = MIN(self.bounds.size.width, self.bounds.size.height);
    _button.frame = CGRectMake(0, 0, side, side);
    _button.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    
    CALayer *layer = _button.layer;
    layer.masksToBounds = YES;
    layer.cornerRadius = side/2;
    layer.borderColor = _strokeColor.CGColor;
    layer.borderWidth = _strokeWidth;
}

- (UITapGestureRecognizer *) backgroundTapRecognizer {
    
    if (_backgroundTapRecognizer == nil) {
        _backgroundTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:nil action:nil];
        _backgroundTapRecognizer.numberOfTapsRequired = 1;
        _backgroundTapRecognizer.numberOfTouchesRequired =1;
        
        [self addGestureRecognizer: _backgroundTapRecognizer];
    }
    
    return _backgroundTapRecognizer;
}

@end

