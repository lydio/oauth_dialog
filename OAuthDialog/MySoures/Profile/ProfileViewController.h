//
//  ProfileViewController.h
//  OAuthDialog
//
//  Created by apple on 10/11/16.
//  Copyright © 2016 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileView;
@interface ProfileViewController : UIViewController

@property (nonatomic, readonly) ProfileView *profileView;

@end
