var http = require('http');
var url = require('url');
var querystring = require('querystring');


var app = http.createServer(function(request, response) {

    var urlComponents = url.parse(request.url, true);
    var pathname = urlComponents.pathname; 
    var query = urlComponents.query; 

    //console.log('pathname: ', pathname);
    //console.log('query: ', query);

    if(pathname == '/registration') {
        
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write('This is test. Registration completed successfully.');
    
    } else if(pathname == '/profile') {

        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write('This is test. Server did change your prifile data.');

    } else if(pathname == '/redirect' && query && query.redirect_url) { 
        
        var redirect_url = query.redirect_url;
        delete query.redirect_url;

        var parameters = '?' + querystring.stringify(query);
        if(parameters.length > 1) {
            redirect_url = redirect_url + parameters;
        }

        response.writeHead(302, {'Location': redirect_url});

    } else if(0 < (pathname.indexOf('://') - pathname.indexOf('redirect/'))) {

        var redirect_url = pathname.substring(pathname.indexOf('redirect/') + 'redirect/'.length);
        
        if(query) {
            var parameters = '?' + querystring.stringify(query);
            if(parameters.length > 1) {
                redirect_url = redirect_url + parameters;
            }
        }
        
        response.writeHead(302, {'Location': redirect_url});
        
    } else {

        response.writeHead(400, {'Content-Type': 'text/plain'});
        response.write('This is test. Error by default.');
    }

    response.end();
});

app.listen(process.env.PORT || 5050); //app.listen(process.env.PORT);
console.log('Server is started');